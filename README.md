# viadkim

The viadkim project has moved to the [Codeberg] platform:

<https://codeberg.org/glts/viadkim>

Please update your links.

[Codeberg]: https://codeberg.org
